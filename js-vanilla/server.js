const { Console } = require("console");

console.log("Hello crocos!");

let var1 = 9;
var1 = "margi";
console.log(var1);

const var2 = 4;
// var2 = 9 => eroare
console.log(var2);

var var3 = "crocos";

// FUNCTII
function greeting(name, age) {
  if (name == undefined && age == undefined) {
    console.log("Hello World!");
  } else if (age == undefined) {
    console.log("hello " + name);
  } else {
    console.log("hello " + name + "; age: " + age);
  }
}

greeting();
greeting("Margi");
greeting("Croco", 15);

let sum = function (a, b) {
  return a + b;
};

console.log(sum(2, 3));
let x = sum(4, 4);
console.log(x);

let arrowFunction = (n) => {
  return (n * (n + 1)) / 2;
};

console.log(arrowFunction(10));

//vectori si metode principale
let array = [12, 3, "Banana", 9.0];

console.log(array[2]);

array.push(10);
console.log(array);
array.pop();
console.log(array);
array.splice(2, 0, "Ana");
console.log(array);

let propozitie = "Imi place sa codez.";
let cuvinte = propozitie.split(" ");
console.log(cuvinte);
let fructe = "banana,mar,para";
let fructeVector = fructe.split(",");
console.log(fructeVector);

//structuri conditionale
if (2 == 1) {
  console.log("egale");
} else {
  console.log("nu sunt egale");
}

//structuri repetitive
for (let i = 0; i < fructeVector.length; i++) {
  console.log(fructeVector[i]);
}

//obiecte / json
let obiect = {
  nume: "Tania",
  varsta: 20,
  adresa: ["Bucuresti", "Bacau"],
  data: {
    zi: 25,
    luna: 11,
    an: 2021,
  },
};

obiect.nume = "Croco";
console.log(obiect);

obiect.data.luna = 12;
console.log(obiect);

for (let i = 0; i < 5; i++) {
  console.log("///////////////////////////////////////////////////////////////////////////////");
}
// <---------------- TEMA 1 --------------------------------------------------->

let rezervari = {
  numarRezervariTotale: 4,
  numePersoane: ["Croco", "Tania", "Adrian", "Mihnea"],
  nrPersoaneLaMasa: [6, 2, 3, 10],
};

let test = 10;

console.log(rezervari);

let sumPersoane = (obj) => {
  let sum = 0;
  let listaRezervari = [];

  if (obj.nrPersoaneLaMasa) {

    for (let i = 0; i < obj.nrPersoaneLaMasa.length; i++) {
      listaRezervari.push(obj.numePersoane[i] + "-" + obj.nrPersoaneLaMasa[i]);
      sum += obj.nrPersoaneLaMasa[i];
    }

    return "Nr total persoane: " + sum + ".\nLista rezervari: " + listaRezervari;
  } else {
    return "kaput";
  }
}


let student = {
  nume: "gogu",
  prenume: "gigel",
  varsta: 15,
  adresa: "strada strazii",
  telefon: 0770474272
};

for (let atr in student) {
  console.log(atr);
}

let pisici = "randomshit,";

console.log(pisici[pisici.length - 1]);