const crocodil = require("./crocodil");
const insula = require("./insula");

const controllers = {
    crocodil,
    insula,
};

module.exports = controllers;