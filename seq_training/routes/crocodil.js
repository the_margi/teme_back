const express = require("express");
const router = express.Router();
const crocodilController = require("../controllers").crocodil;

router.get("/", crocodilController.getAllCrocodiles);
router.get("/:id", crocodilController.getCrocodileById);
router.post("/", crocodilController.addCrocodile);
router.put("/:id", crocodilController.updateCrocodile);
router.delete("/:id", crocodilController.deleteCrocodileById);

module.exports = router;